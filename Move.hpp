#ifndef MOVE_H
#define MOVE_H

#include <string>
#include "Typing.hpp"

class Move {

	public:
		std::string name, summary;
		int power;
		Typing::id type;

		Move();
		Move(std::string, std::string, int, Typing::id);
		Move(const Move &m);
		~Move();

		std::string str() const;

		//
		static std::vector<Move *> movepool;

};

#endif // MOVE_H