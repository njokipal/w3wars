#include "ANSI.hpp"
#include "globals.hpp"

#ifdef WIN32
#include <windows.h>

	std::ostream & ANSI::handleTerminal(std::ostream &os, Code c) {

		/*
		black=0,
		dark_blue=1,
		dark_green=2,
		dark_aqua,dark_cyan=3,
		dark_red=4,
		dark_purple=5,dark_pink=5,dark_magenta=5,
		dark_yellow=6,
		dark_white=7,
		gray=8,
		blue=9,
		green=10,
		aqua=11,cyan=11,
		red=12,
		purple=13,pink=13,magenta=13,
		yellow=14,
		white=15
		*/
		HANDLE hConsole;
		int k;

		switch(c) {
			case 39: k = 0; break;
			case 30: k = 0; break;
			case 31: k = 4; break;
			case 32: k = 2; break;
			case 33: k = 6; break;
			case 34: k = 1; break;
			case 35: k = 13; break;
			case 36: k = 11; break;
			case 37: k = 8; break;
			default: k = 0; break;
		}

		hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hConsole, k);

		return os << k << " ";

	}

#else

	std::ostream & ANSI::handleTerminal(std::ostream &os, Code c) {

		const std::string tname = getenv("TERM");
		const std::string supported[] = {"xterm", "gnome-terminal", "guake", "konsole", "terminator", "vte"};

		if (gbl::in_array(tname, supported)) {
			return os << "\033[" << static_cast<int>(c) << "m"; // \033[0m
		}

		return os;

	}

#endif // WIN32

const std::string ANSI::operator+(const std::string &s, const Code &c) {

	std::ostringstream o;
	handleTerminal(o, c);

	return s + o.str();

}

const std::string ANSI::operator+(const Code &c, const std::string &s) {

	std::ostringstream o;
	handleTerminal(o, c);

	return o.str() + s;

}

std::ostream& ANSI::operator<<(std::ostream &os, Code c) {

	return handleTerminal(os, c);

}