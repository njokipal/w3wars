#ifndef URL_H
#define URL_H

#include <string>
#include "URLException.hpp"

class URL {

	public:
		URL();
		URL(std::string) throw (URLException);
		~URL();

		std::string url, protocol, host, path, port, query;
		bool valid;

		static const std::string URL_FORMAT_GENERAL;
		static const std::string URL_FORMAT_IPV4;
		static const std::string ACCEPTED_PROTOCOLS[];

	private:
		void parse() throw (URLException);

};

#endif // URL_H