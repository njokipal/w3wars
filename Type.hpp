#ifndef TYPE_H
#define TYPE_H

#include "Typing.hpp"

class Type {

	public:
		int id;

		Type();
		~Type();

		void setType(Typing::id);

		double coeff(int, int) const;
		std::string str() const;

};

#endif // TYPE_H