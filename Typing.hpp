#ifndef TYPING_H
#define TYPING_H

#include <string>
#include <vector>

namespace Typing {

	enum id {

		GENERAL, // general
		BLOG, // blog
		WIKI, // wiki
		NEWS, // news
		//YELLOW_PRESS, // yellow press
		SOCIAL_MEDIA, // social media
		//SPORTS, // sports
		ENTERTAINMENT, // media
		SEARCH // search engine

	};

	const int LENGTH = 7;

	const std::string name[] = {"General", "Blog", "Information", "News", "Social Media", "Entertainment", "Search Engine"};

	// http://www.alexa.com/topsites/category/Top
	const std::string url_regex[] = {"", "blogger\\.com|wordpress\\.org|squarespace\\.com|hubpages\\.com|typepad\\.com|today\\.com|blog\\.com|blogspot\\.fi", "howstuffworks\\.com|wikipedia\\.org|stackoverflow\\.com|answers\\.yahoo\\.com|thesaurus\\.com|archive\\.org|stumbleupon\\.com|nlm\\.nih\\.gov|goodreads\\.com|wordreference\\.com|thefreedictionary\\.com|urbandictionary\\.com|merriam-webster\\.com|wiktionary\\.org|www\\.udemy\\.com|blackboard\\.com|w3\\.org|maps\\.google\\.com|www\\.coursera\\.org|investopedia\\.com|dict\\.cc|brainyquote\\.com", "yle\\.fi\\/uutiset|hs\\.fi|ksml\\.fi|reddit\\.com|news\\.yahoo\\.com|cnn\\.com|huffingtonpost\\.com|nytimes\\.com|news\\.google\\.com|weather\\.com|theguardian\\.com|forbes\\.com|foxnews\\.com|bbc\\.co\\.uk\\/news|washingtonpost\\.com|shutterstock\\.com|online\\.wsj\\.com|usatoday\\.com|time\\.com|reuters\\.com|accuweather\\.com|bloomberg\\.com|individual\\.com|abcnews\\.go\\.com", "facebook\\.com|twitter\\.com|linkedin\\.com|plus\\.google\\.com|pinterest\\.com|flickr\\.com|livejournal\\.com|badoo\\.com|stumbleupon\\.com|hootsuite\\.com|fiverr\\.com|meetup\\.com|okcupid\\.com|tagged\\.com|xing\\.com|foursquare\\.com|ning\\.com|webs\\.com|last\\.fm|myspace\\.com|hi5\\.com|skyrock\\.com|classmates\\.com|care2\\.com", "youtube\\.com|espn\\.com|deviantart\\.com|imdb\\.com|spotify\\.com|rottentomatoes\\.com", "google\\.[a-z\\.]{2,5}|bing\\.com|yahoo\\.com|msn\\.com|baidu\\.com"};
	const std::string keywords[] = {"", "blog|journal", "wiki|map|dictionary|book|knowledge|kartta|(sana|)kirja|tieto", "news|uutiset", "community|yhteisö", "entertainment|soccer|football|hockey|sports|movies?|videos?|porn|viihde", ""};

}

#endif // TYPING_H