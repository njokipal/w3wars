#ifndef ANSI_H
#define ANSI_H

#include <ostream>
#include <sstream>
#include <algorithm>
#include <string>

namespace ANSI {

	enum Code {

		NORMAL = 0,

		IMAGE_NEGATIVE = 7,
		IMAGE_NORMAL = 27,

		WEIGHT_BOLD = 1,
		WEIGHT_THIN = 2,
		WEIGHT_NORMAL = 22,

		STYLE_ITALIC = 1,
		STYLE_NORMAL = 23,

		DECORATION_UNDERLINED = 4,
		DECORATION_NONE = 24,

		FONT_DEFAULT = 10,
		//FONT_ALT_1 = 11,

		FG_DEFAULT = 39,
		FG_BLACK = 30,
		FG_RED = 31,
		FG_GREEN = 32,
		FG_YELLOW = 33,
		FG_BLUE = 34,
		FG_MAGENTA = 35,
		FG_CYAN = 36,
		FG_LIGHT_GRAY = 37,
		FG_DARK_GRAY = 90,
		FG_LIGHT_RED = 91,
		FG_LIGHT_GREEN = 92,
		FG_LIGHT_YELLOW = 93,
		FG_LIGHT_BLUE = 94,
		FG_LIGHT_MAGENTA = 95,
		FG_LIGHT_CYAN = 96,
		FG_WHITE = 97,

		BG_DEFAULT = 49,
		BG_RED = 41,
		BG_GREEN = 42,
		BG_BLUE = 44

	};

	std::ostream & handleTerminal(std::ostream &, Code);
	const std::string operator+(const std::string &, const Code &);
	const std::string operator+(const Code &, const std::string &);
	std::ostream& operator<<(std::ostream &, Code);

}

#endif // ANSI_H