#include <cstdio>
#include <cstdlib>
#include <errno.h>
#include <iostream>
#include <cstring>
#include <string>
#include <sstream> // stringstream
#include <thread> // this_thread

#ifndef WIN32
#include <sys/time.h>
#endif

#include <sys/types.h> // data types
#include <sys/stat.h> // file data
#include <fcntl.h> // open

#ifdef WIN32
#include <io.h>
#else
#include <unistd.h> // posix api
#endif

#include "globals.hpp"
#include "URL.hpp"
#include "URLException.hpp"
#include "Page.hpp"
#include "ANSI.hpp"
#include "Move.hpp"

using namespace std;

void loadPages(Page *, Page *);
void populateMovepool();

//class Move;

int main(int argc, char *argv[]) {

	/* $ g++ main.cc globals.cc ANSI.cc Move.cc Type.cc URL.cc URLException.cc Page.cc -lcurl -pthread -std=c++11 -o w3wars
	 */

	srand(time(NULL));
	bool verbose = false;

	//
	Page *page1 = NULL;
	Page *page2 = NULL;

	try {
		if (argc == 1) gbl::print_help();
		else {
			for(int i = 1; i < argc; i++) {
				char* arg = argv[i];

				if (arg[0] == '-') {
					if (strchr(arg, gbl::FLAG_HELP) != NULL) gbl::print_help();
					else {
						if (strchr(arg, gbl::FLAG_DEFAULT) != NULL) {
							page1 = new Page(gbl::DEFAULT_URL1, 1);
							page2 = new Page(gbl::DEFAULT_URL2, 0);
						}
						if (strchr(arg, gbl::FLAG_VERBOSE) != NULL) verbose = true;
					}
				}
				else {
					if (page1 == NULL) {
						page1 = new Page(arg, 1);
					}
					else {
						if (page2 == NULL) {
							page2 = new Page(arg, 0);
						}
					}
				}
			}

			if (page1 == NULL || page2 == NULL) {
				throw(new int);
			}
		}
	}
	catch (URLException e) {
		gbl::error(e.getMessage(), NULL);
	}
	catch (...) {
		gbl::error("ei tarpeeksi URLeja!", NULL);

		return EXIT_FAILURE;
	}

	//
	populateMovepool();

	cout << "Ladataan sivuja " << flush;

	thread l(loadPages, page1, page2);

	bool ready = false;
	char cursor[4] = {'/','-','\\','|'};
	int c = 0;
    while (!ready) {
		printf("%c\b", cursor[c]);
		cout << flush;

		this_thread::sleep_for(chrono::milliseconds(200));

        c = (c+1)%4;
        ready = (page1->ready && page2->ready);
    }

	cout << "\b\t" << ANSI::FG_GREEN << "OK" << ANSI::NORMAL;

    l.join();

	//
	cout << endl << endl << "LADATUT SIVUT:" << endl;

	cout << page1 << endl << page1->getInfo() << endl;
	cout << page2 << endl << page2->getInfo() << endl;

	cout << "(paina <ENTER>)";
	cin.get();

	cout << gbl::clear();

	int round = 1;
	do {
		cout << string(2, '\n');//gbl::clear();

		cout << "Kierros #" << round << endl <<
				page1->strMin() << page2->strMin() << endl;

		page1->planRound(page2);

		if (!page1->menu) {
			page2->planRound(page1);

			if (page1->attacking && page2->attacking) {
				cout << "Molemmat iskevät! ";

				if (page1->stats.speed > page2->stats.speed) {
					cout << page1->name << " oli nopeampi! " << endl;
					page1->battleRound(page2);
				}
				else {
					cout << page2->name << " oli nopeampi! " << endl;
					page2->battleRound(page1);
				}
			}
			else if (page1->defending && page2->defending) {
				cout << "Molemmat estivät... " << endl;
			}
			else if (page1->dodging && page2->dodging) {
				cout << "Molemmat väistivät... " << endl;
			}
			else {
				page1->battleRound(page2);
				page2->battleRound(page1);
			}

			round++;
		}

		page1->endRound();
		page2->endRound();
	} while(!page1->fainted && !page2->fainted);

	cout << "Ottelu päättyi!" << endl;

	if (page1->fainted && !page2->fainted) {
		cout << page1->name << " pyörtyi. Voittaja on " << ANSI::WEIGHT_BOLD << page2->name << ANSI::WEIGHT_NORMAL << "!" << endl;
	}
	else if (page2->fainted && !page1->fainted) {
		cout << page2->name << " pyörtyi. Voittaja on " << ANSI::WEIGHT_BOLD << page1->name << ANSI::WEIGHT_NORMAL << "!" << endl;
	}
	else {
		cout << "Tasapeli." << endl;
	}

	// tiedostoon tallennus

	//
	return EXIT_SUCCESS;

}

void loadPages(Page *p1, Page *p2) {

	p1->crawl();
	p2->crawl();

}

void populateMovepool() {

	Move::movepool.push_back(new Move("Jaa Facebookissa", "", 30, Typing::GENERAL));
	Move::movepool.push_back(new Move("yhdys sana virhe", "", 30, Typing::GENERAL));
	Move::movepool.push_back(new Move("Ei mobiiliversiota", "", 30, Typing::GENERAL));
	Move::movepool.push_back(new Move("Sheisse rakenne, pitää googlettaa että löytää", "", 30, Typing::GENERAL));

	Move::movepool.push_back(new Move("Arkihaaste", "", 30, Typing::SOCIAL_MEDIA));
	Move::movepool.push_back(new Move("Muka-yhteiskuntakritiikillinen vihapuhe", "", 30, Typing::SOCIAL_MEDIA));
	Move::movepool.push_back(new Move("Jaettu kissavideo", "", 30, Typing::SOCIAL_MEDIA));
	Move::movepool.push_back(new Move("Tilanpäivitys: \"Kiva päivä tänään :)\"", "", 30, Typing::SOCIAL_MEDIA));
	Move::movepool.push_back(new Move("#kaikki #sanat #tägeinä", "", 30, Typing::SOCIAL_MEDIA));
	Move::movepool.push_back(new Move("Tilapäivitys: \"mistä tietää onko lapsi saatananpalvoja\"", "", 30, Typing::SOCIAL_MEDIA));
	Move::movepool.push_back(new Move("Mikä Muumit-hahmo olet?", "", 30, Typing::SOCIAL_MEDIA));
	Move::movepool.push_back(new Move("Belfie", "", 30, Typing::SOCIAL_MEDIA));
	Move::movepool.push_back(new Move("vau.fi-trollaus", "", 30, Typing::SOCIAL_MEDIA));
	Move::movepool.push_back(new Move("Suomi24-keskustelu", "", 30, Typing::SOCIAL_MEDIA));

	Move::movepool.push_back(new Move("Ei tänäänkään mitään uutta", "", 30, Typing::BLOG));
	Move::movepool.push_back(new Move("\"Ammatti\"bloggarin sponsori-huoraus", "", 30, Typing::BLOG));
	Move::movepool.push_back(new Move("Herkullisen suklaakakun resepti", "", 30, Typing::BLOG));
	Move::movepool.push_back(new Move("Masu-asukin oma blogi", "", 30, Typing::BLOG));
	Move::movepool.push_back(new Move("Joku aivoton #fitness-postaus", "", 30, Typing::BLOG));
	Move::movepool.push_back(new Move("Kuvia meidän IKEA-kalusteista", "", 30, Typing::BLOG));

	Move::movepool.push_back(new Move("Erä CS:ää", "", 30, Typing::ENTERTAINMENT));
	Move::movepool.push_back(new Move("Kissavideo", "", 30, Typing::ENTERTAINMENT));
	Move::movepool.push_back(new Move("Makutestissä ES-energiajuoma", "", 30, Typing::ENTERTAINMENT));
	Move::movepool.push_back(new Move("Vapaita Sinkkuja Lähellä Sijaintia Jyv%C3%A4skyl%C3%A4", "", 30, Typing::ENTERTAINMENT));
	Move::movepool.push_back(new Move("MUN - MCI", "", 30, Typing::ENTERTAINMENT));
	Move::movepool.push_back(new Move("Nettipokeriturnaus", "", 30, Typing::ENTERTAINMENT));
	Move::movepool.push_back(new Move("Salil eka, salil vika", "", 30, Typing::ENTERTAINMENT));

	Move::movepool.push_back(new Move("USA-uutinen", "", 30, Typing::NEWS));
	Move::movepool.push_back(new Move("Katso kuvat!", "", 30, Typing::NEWS));
	Move::movepool.push_back(new Move("\"Kylän Pentti sai kuhan\"", "", 30, Typing::NEWS));
	Move::movepool.push_back(new Move("Säätiedote: huomennakin räntää", "", 30, Typing::NEWS));
	Move::movepool.push_back(new Move("Talousuutiset", "", 30, Typing::NEWS));

	Move::movepool.push_back(new Move("Omniboxiin \"facebook\" -> klikkaa ensimmäistä hakutulosta", "", 30, Typing::SEARCH));
	Move::movepool.push_back(new Move("\"hapan silakka ohje\"", "", 30, Typing::SEARCH));
	Move::movepool.push_back(new Move("bing.com -> \"google\"", "", 30, Typing::SEARCH));
	Move::movepool.push_back(new Move("Ei osumia.", "", 30, Typing::SEARCH));
	Move::movepool.push_back(new Move("Tarkoititko \"peräpukamavoide\"?", "", 30, Typing::SEARCH));

	Move::movepool.push_back(new Move("Suositeltu artikkeli", "", 30, Typing::WIKI));
	Move::movepool.push_back(new Move("Perusteellinen artikkeli", "", 30, Typing::WIKI));
	Move::movepool.push_back(new Move("Viittaus ö-tason tutkimukseen", "", 30, Typing::WIKI));
	Move::movepool.push_back(new Move("[lähde?]", "", 30, Typing::WIKI));
	Move::movepool.push_back(new Move("error: stackoverflow", "", 30, Typing::WIKI));
	Move::movepool.push_back(new Move("Joulukinkun paisto-ohje", "", 30, Typing::WIKI));

}