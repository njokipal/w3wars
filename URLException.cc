#include "URLException.hpp"

URLException::URLException(std::string m) { this->msg = m; }

URLException::~URLException() {}

const char* URLException::getMessage() const { return this->msg.c_str(); }