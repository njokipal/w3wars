#include <iostream>
#include <string>
#include <algorithm> // find, search
#include "URL.hpp"
#include "URLException.hpp"

const std::string URL::URL_FORMAT_GENERAL = "protocol://sub.domain.ending:port/path?query";
const std::string URL::URL_FORMAT_IPV4 = "protocol://[0-255].[0-255].[0-255].[0-255]:port/path?query";
const std::string URL::ACCEPTED_PROTOCOLS[] = {"http", "https"};

//

URL::URL() {}

URL::URL(std::string u) throw (URLException) : url(u), protocol(""), host(""), path(""), port(""), query("")  {

	this->parse();

}

URL::~URL() {}

void URL::parse() throw (URLException) {

	//std::cout << "parsitaan " << this->url << std::endl;

	const std::string tmp = this->url;

	if (tmp.empty()) {
		this->valid = false;

		throw(URLException("URL on tyhjä"));
	}
	else {
		const char* protocol_end_str = "://";
		const char path_start_char = '/';
		const char port_start_char = ':';
		const char query_start_char = '?';

		std::string::const_iterator tmp_start = tmp.begin();
		std::string::const_iterator tmp_end = tmp.end();

		std::string::const_iterator query_start = std::find(tmp_start, tmp_end, query_start_char);
		std::string::const_iterator query_end = tmp_end;

		std::string::const_iterator protocol_start = tmp_start;
		std::string::const_iterator protocol_end = std::find(protocol_start, tmp_end, port_start_char);

		// protocol
		if (protocol_end != tmp_end) {
			std::string prot = &*(protocol_end);

			if ((prot.length() > 3) && (prot.substr(0, 3) == protocol_end_str)) {
				prot = std::string(protocol_start, protocol_end);
				if (std::find(std::begin(URL::ACCEPTED_PROTOCOLS), std::end(URL::ACCEPTED_PROTOCOLS), prot) != std::end(URL::ACCEPTED_PROTOCOLS)) this->protocol = prot;
				else throw(URLException("Tätä protokollaa ("+prot+") ei tueta!"));

				protocol_end += +3; // ://
			}
			else protocol_end = tmp_start;  // no protocol
		}
		else protocol_end = tmp_start;

		std::string::const_iterator host_start = protocol_end;

		std::string::const_iterator path_start = std::find(host_start, tmp_end, path_start_char);

		std::string::const_iterator host_end = std::find(protocol_end, ((path_start != tmp_end) ? path_start : query_start), port_start_char);

		host = std::string(host_start, host_end);
		if (host != "localhost" && host != "127.0.0.1") this->host = host;
		else {
			this->valid = false;

			throw(URLException("lähiverkkoa ei tueta!"));
		}

		// port
		if ((host_end != tmp_end) && ((&*(host_end))[0] == port_start_char)) {
			host_end++;
			std::string::const_iterator port_end = ((path_start != tmp_end) ? path_start : query_start);

			this->port = std::string(host_end, port_end);
		}

		// path
		if (path_start != tmp_end) this->path = std::string(path_start, query_start);

		// query
		if (query_start != tmp_end) this->query = std::string(query_start, tmp_end);

		this->valid = true;
	}

}