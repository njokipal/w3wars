#ifndef URL_EXCEPTION_H
#define URL_EXCEPTION_H

#include <string>

class URLException {

	public:
		URLException();
		URLException(std::string);
		~URLException();

		const char* getMessage() const;

	private:
		std::string msg;

};

#endif // URL_EXCEPTION_H