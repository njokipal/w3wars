#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>

#include "globals.hpp"
#include "URL.hpp"
#include "ANSI.hpp"

void gbl::error(const char *msg, const char *f, ...) {

	std::cout << std::endl;

	fprintf(stderr, msg, f);

	std::cout << "Lopetetaan..." << std::endl;

	//perror(msg);
	::exit(1);

}

void gbl::print_help() {

	std::cout << ANSI::WEIGHT_BOLD << gbl::NAME << ANSI::WEIGHT_NORMAL << " - " << gbl::SUMMARY << std::endl <<
			"KÄYTTÖ" << gbl::tab << ANSI::WEIGHT_BOLD << gbl::CMD << ANSI::WEIGHT_NORMAL << " <url...> <url...> [liput]" << std::endl << std::endl <<
			"KUVAUS" << gbl::tab << "URL'ja tulee olla kaksi, kummatkin muotoa " << ANSI::DECORATION_UNDERLINED << URL::URL_FORMAT_GENERAL << ANSI::DECORATION_NONE << " (general)" << std::endl <<
						gbl::tab << "tai " << URL::URL_FORMAT_IPV4 << " (IPv4)," << std::endl <<
						gbl::tab << "jossa protokolla voi olla http tai https, alidomain voi olla olematta tai useita," << std::endl <<
						gbl::tab << "pääte mikä tahansa tld tms. ja portti voi olla olematta." << std::endl << std::endl <<
			"LIPUT"	<< 	gbl::tab << "Liput alkavat merkillä " << ANSI::WEIGHT_BOLD << "-" << ANSI::WEIGHT_NORMAL << " ja lippuja voi olla useita." << std::endl <<
						gbl::tab << '-' << gbl::FLAG_HELP << gbl::tab << " (help) tulostetaan tämä näkymä" << std::endl <<
						gbl::tab << '-' << gbl::FLAG_VERBOSE << gbl::tab << " (verbose) näytetään tulostukset kaikista vaiheista" << std::endl <<
						gbl::tab << '-' << gbl::FLAG_DEFAULT << gbl::tab << " (defaults) käytetään oletusarvoja URL'lle" << std::endl << std::endl;

	::exit(1);

}

std::string gbl::clear() {

	return std::string(50, '\n');//std::string("\n\n * * * * * \n\n");

}

std::size_t gbl::write_data(char *ptr, size_t size, size_t nmemb, void *outstream) {

	std::ostringstream *stream = (std::ostringstream *)outstream;
    std::size_t count = size * nmemb;
    stream->write(ptr, count);

    return count;

}

//~ size_t gbl::read_callback(void *ptr, size_t size, size_t nmemb, void *instream) {
//~
	//~ std::cout << instream;
	//~ curl_off_t nread;
	//~ /* in real-world cases, this would probably get this data differently
	//~ as this fread() stuff is exactly what the library already would do
	//~ by default internally */
	//~ size_t retcode = fread(ptr, size, nmemb, (FILE *)instream);
//~
	//~ nread = (curl_off_t)retcode;
//~
	//~ fprintf(stderr, "*** We read %" CURL_FORMAT_CURL_OFF_T " bytes from file\n", nread);
//~
	//~ return retcode;
//~
	//~ return 0;
//~
//~ }