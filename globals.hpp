#ifndef GLOBAL_H
#define GLOBAL_H

#include <algorithm> // find
#include <string>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
	static const char envi = 'w'; // WIN
#else
	static const char envi = 'u'; // UNIX
#endif

namespace gbl {

	const std::string NAME = "W3Wars - HARKKATYÖ";
	const std::string SUMMARY = "olio-ohjelmointi 2:n harkkatyö, (c) Niko Jokipalo 2014";
	const std::string CMD = "./w3wars";
	const std::string UA = "W3WarsCrawler(c)NJ2014";

	const std::string DEFAULT_URL1 = "http://youtube.com";
	const std::string DEFAULT_URL2 = "https://google.fi/";

	const char tab = '\t';

	const char FLAG_HELP = 'h';
	const char FLAG_VERBOSE = 'v';
	const char FLAG_DEFAULT = '0';

	//bool verbose = false;

	void error(const char *, const char *, ...);
	void print_help();

	std::string clear();

	std::size_t write_data(char *, size_t, size_t, void *);
	//size_t read_callback(void *, size_t, size_t, void *);

	template<class T, class C>
	bool in_array(const T &elm, const C &cner) {

		return (std::find(std::begin(cner), std::end(cner), elm) != std::end(cner));

	}

}

#endif // GLOBAL_H