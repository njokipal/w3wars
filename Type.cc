#include "Type.hpp"
#include "Typing.hpp"

Type::Type() : id(Typing::GENERAL) {}

Type::~Type() {}

void Type::setType(Typing::id i) {

	this->id = i;

}

double Type::coeff(int a, int d) const {

	double c[Typing::LENGTH][Typing::LENGTH] = {{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},
												{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},
												{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},
												{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},
												{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},
												{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},
												{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0}};

	return c[a][d];

}

std::string Type::str() const {

	return Typing::name[this->id];

}