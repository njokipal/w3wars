#include <vector>
#include "globals.hpp"
#include "Move.hpp"
#include "Typing.hpp"

Move::Move(std::string n, std::string s, int p, Typing::id t) {

	this->name = n;
	this->power = p;
	this->summary = s;
	this->type = t;

}

Move::Move(const Move &m) {

	*this = m;

}

Move::~Move() {}

std::string Move::str() const {

	return this->name + " (" + std::to_string(this->power) + ")";

}

//
std::vector<Move *> Move::movepool;