#include <iostream>
#include <sstream>
#include <string>
#include <chrono>
#include <ctime>
#include <cmath>
#include <regex>
#include <algorithm>
#include <map>

#include <curl/curl.h>

#include "globals.hpp"
#include "Page.hpp"
#include "Stats.hpp"
#include "Type.hpp"
#include "Typing.hpp"
#include "ANSI.hpp"
#include "Move.hpp"

Page::Page() {}

Page::Page(std::string u, int p) : location(new URL(u)), player(p) {

	//std::cout << "Page luotu" << std::endl;

	this->ready = false;

	this->type = Type();

	this->secure = false;
	this->loadtime = 0.0; // s
	this->filesize = 0.0; // b

	this->stats.hp = 100;
	this->stats.hpMax = 100;
	this->stats.attack = 1;
	this->stats.defence = 1;
	this->stats.speed = 1;
	this->stats.luck = 1;

	this->menu = false;
	this->fainted = false;
	this->attacking = false;
	this->defending = false;
	this->dodging = false;
	this->attackIndex = 0;

	this->moves = std::vector<Move *>();

}

Page::~Page() {

	delete this->location;

}

/* kutsuu Page-olion ostream-tulostusta
 * name: operator<<
 * @param viittaus ostream, pointteri Page-olioon
 */
std::ostream & operator<<(std::ostream &os, const Page *p) {

	return os << *p;

}

/* tulostaa Page-olion str()-tarjoaman tekstin ostreamiin
 * name: operator<<
 * @param viittaus ostream, viittaus Page-olioon
 *
 */
std::ostream & operator<<(std::ostream &os, const Page &p) {

	return os << p.str(0);

}

std::string Page::str(int c = 0) const {

	std::string nu = std::string(this->name + " (" + this->location->url + ")\n"),
				nh = std::string(this->name + " " + this->getHP() + " \n"), // nimi ja hp
				t = std::string("\tTYPE\t" + this->type.str() + " \n"),
				h = std::string("\tHP\t")+ this->getHP() + " \n",
				w = std::string("\tWEIGHT\t" + std::to_string(this->stats.weight) + " kg\n"),
				s = std::string("\tSPEED\t" + std::to_string(this->stats.speed) + " km/h\n"),
				a = std::string("\tATTACK\t" + std::to_string(this->stats.attack) + " \n"),
				d = std::string("\tDEFENCE\t" + std::to_string(this->stats.defence) + " \n"),
				l = std::string("\tLUCK\t" + std::to_string(this->stats.luck) + " \n"),
				p = this->getPlayer();

	switch(c) {
		default: return p+nu+t+h+w+s+a+d+l;
		case 1: return p+nh;
	}

}

std::string Page::getHP() const {

	ANSI::Code hpColour;
	double hpRatio = ((double)this->stats.hp/this->stats.hpMax);
	if (hpRatio < .2) hpColour = ANSI::FG_RED;
	else if (hpRatio < .5 && hpRatio >= .2) hpColour = ANSI::FG_YELLOW;
	else hpColour = ANSI::FG_GREEN;

	std::string hp = hpColour + std::to_string(this->stats.hp)+ " / "+ std::to_string(this->stats.hpMax) + ANSI::FG_DEFAULT;

	return hp;

}

std::string Page::getPlayer() const {

	std::string p = (this->player == 1 ? ANSI::BG_GREEN + std::string("[PELAAJA]") + ANSI::NORMAL : std::string("[CPU]\t")) + '\t';

	return p;

}

std::string Page::strMin() const {

	return this->str(1);

}

std::string Page::getInfo() const {

	std::string k = std::string("KEYWORDS\t" + this->keywords + " \n"),
				d = std::string("DESCRIPTION\t" + this->description + " \n");

	return k+d;

}

void Page::setHP(int d) {

	this->stats.hp += d;

	this->fainted = (this->stats.hp <= 0);

}

/* yhdistää annettuun URL-osoitteeseen, lataa HTTP-otsakkeet ja tallentaa HTML-sisällön string-muuttujaan
 * name: crawl
 * @param
 * @return
 *
 */
void Page::crawl() {

	//static const char *filename1 = "url1.out";
	//static const char *filename2 = "body.out";
	//FILE *file1;
	//FILE *bodyfile;

	std::ostringstream stream, stream2;

	std::chrono::steady_clock::time_point start, end;
	std::chrono::duration<double> elapsed;

	CURL *curl;
	CURLcode res;

	curl = curl_easy_init();
	if (curl) {
		//std::cout << "yhdistetään " << this->location->url.c_str() << "..." << std::endl;

		curl_easy_setopt(curl, CURLOPT_URL, this->location->url.c_str());
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, gbl::write_data);
		//curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);
		curl_easy_setopt(curl, CURLOPT_HEADERDATA, &stream2);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &stream);
		curl_easy_setopt(curl, CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS); // sallittuja protokollia vain http(s)
		curl_easy_setopt(curl, CURLOPT_CERTINFO, 1L); // lataa sertifikaattien tiedot
		curl_easy_setopt(curl, CURLOPT_USERAGENT, gbl::UA.c_str());

		//curl_easy_setopt(curl_handle, CURLOPT_HEADERDATA, headerfile); /* we want the headers be written to this file handle */
		//curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, bodyfile); /* we want the body be written to this file handle instead of stdout */

		start = std::chrono::steady_clock::now();// timer start

		res = curl_easy_perform(curl);

		if (res != CURLE_OK) {
			gbl::error("cURL-virhe: %s\n", curl_easy_strerror(res));
		}
		//else std::cout << "onnistui: " << curl_easy_strerror(res) << std::endl;

		// testataan, onko secure @ http://curl.haxx.se/libcurl/c/certinfo.html
		if (this->location->url.find(std::string("s://")) != std::string::npos) {
			union {
				struct curl_slist *to_info;
				struct curl_certinfo *to_certinfo;
			} ptr;

			ptr.to_info = NULL;

			res = curl_easy_getinfo(curl, CURLINFO_CERTINFO, &ptr.to_info);

			if (res == CURLE_OK && ptr.to_info) {
				if (ptr.to_certinfo->num_of_certs > 0) this->secure = true;
			}
		}
		//
		end = std::chrono::steady_clock::now(); // timer end
		elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end-start);

		this->loadtime = elapsed.count();

		this->parseHeaders(stream2);
		this->parseSource(stream);
		this->determineType();
		this->determineStats();
		this->determineMoves();

		this->ready = true;

		//
		curl_easy_cleanup(curl);
	}

}
void Page::parseSource(const std::ostringstream &s) {

	const std::string source(s.str());
	std::string head;
	const std::string head_start_str = "<head>", head_end_str = "</head>";

	std::string::size_type head_start = source.find(head_start_str), head_end = source.find(head_end_str);
	head = source.substr(head_start, head_end);
	std::regex	title_regex("<title[^>]*>(.*)</title>", std::regex_constants::ECMAScript | std::regex_constants::icase),
				desc_regex("<meta name=\"description\" content=\"([^\"]*)\".*>", std::regex_constants::ECMAScript | std::regex_constants::icase),
				keywords_regex("<meta name=\"keywords\" content=\"([^\"]*)\".*>", std::regex_constants::ECMAScript | std::regex_constants::icase);
	std::smatch match;

	if (std::regex_search(head, match, title_regex)) {
		this->name = std::string(match[1]);
	}

	std::string desc, kws;
	if (std::regex_search(head, match, desc_regex)) {
		this->description = std::string(match[1]);
	}

	if (std::regex_search(head, match, keywords_regex)) {
		this->keywords = std::string(match[1]);
	}

	this->filesize = source.length();

}
void Page::parseHeaders(const std::ostringstream &h) {

	const std::string data = h.str();

	const std::regex content_length_regex("Content-Length: ([0-9]{1,})");
	std::smatch match;

	double filesize = 0.0;
	if (std::regex_search(data, match, content_length_regex)) {
		this->filesize = std::stod(match[1]);
		//~ std::cout << "tiedostokoko otsakkeesta: " << match[1] << " b";
	}

}

void Page::determineType() {

	const std::string url = this->location->host;
	const std::string name = this->name;
	const std::string kwds = this->keywords;
	const std::string desc = this->description;

	struct MatchCount {

		int key;
		int count;

		bool operator<(const MatchCount &a) { return !(this->count < a.count); }

	} matches[Typing::LENGTH];

	bool c = false;
	for(int i = 0; i < Typing::LENGTH; i++) {
		matches[i].key = i;
		matches[i].count = 0;

		if (Typing::url_regex[i] != "") {
			std::regex u('('+Typing::url_regex[i]+')', std::regex_constants::ECMAScript | std::regex_constants::icase);

			if (std::regex_search(url, u)) matches[i].count += 5;
		}

		if (Typing::keywords[i] != "") {
			std::regex k('('+Typing::keywords[i]+')', std::regex_constants::ECMAScript | std::regex_constants::icase);

			if (std::regex_search(name, k)) matches[i].count += 2;
			if (std::regex_search(kwds, k)) matches[i].count += 1;
			if (std::regex_search(desc, k)) matches[i].count += 1;
		}

		if (matches[i].count > 0) c = true;
	}

	if (c) {
		std::sort(std::begin(matches), std::end(matches));
		/*for(int i = 0; i < Typing::LENGTH; i++) {
			std::cout << matches[i].key << ": " << matches[i].count << std::endl;
		}*/

		MatchCount *first = std::begin(matches);
		//std::cout << "eniten pisteitä: " << first->key << " (" << first->count << " kpl)" << std::endl;

		this->type.setType((Typing::id)first->key);
	}

}

void Page::determineStats() {

	this->stats.weight = (this->filesize/1000);
	this->stats.speed = ceil((this->stats.weight/this->loadtime)/10);
	this->stats.attack += std::rand()%5;
	this->stats.defence += std::rand()%3;
	if (this->secure) this->stats.defence += 5;

	this->stats.luck += std::rand()%5;

}

void Page::determineMoves() {

	int size = 4-(rand()%2);
	//std::cout << "ladataan liikkeitä " << size << " kpl..." << std::endl;

	while(this->moves.size() < size) {
		int rnd = (rand() % Move::movepool.size());
		std::vector<Move *>::const_iterator it = std::begin(Move::movepool) + rnd;

		if (((*it)->type == Typing::GENERAL || (*it)->type == this->type.id) && !gbl::in_array(*it, this->moves)) this->moves.push_back(*it);
	}

}

void Page::planRound(Page *opponent) {

	int b;

	switch(this->player) {
		case 1:

			std::cout <<	"Mitä haluat tehdä?" << std::endl <<
							"1) hyökkää" << std::endl <<
							"2) puolusta" << std::endl <<
							"3) väistä" << std::endl <<
							//"4) katso statsit" << std::endl <<
							" > ";

			std::cin >> b;
			std::cin.ignore(512, '\n');

		break;
		default:

			b = rand()%3+1;

		break;
	}

	switch(b) {
		case 0: break;
		case 1:

			int a;
			switch(this->player) {
				case 1: {

					std::cout << "Valitse isku:" << std::endl;

					int i = 1;
					for(std::vector<Move *>::const_iterator it = std::begin(this->moves); it != std::end(this->moves); ++it) {
						std::cout << i << ") " << (**it).str() << std::endl;

						i++;
					}

					std::cout << " > ";
					std::cin >> a;
					std::cin.ignore(512, '\n');

					a--;

				} break;
				default: a = rand()%4; break;
			}

			this->attack(a);

		break;
		case 2: default: this->defend(); break;
		case 3: this->dodge(); break;
		case 4: std::cout << this << std::endl << opponent; this->menu = true; break;
	}

}

void Page::battleRound(Page *opponent) {

	std::cout << this->getPlayer() << this->name << ' ';

	if (this->attacking) {
		std::cout << "iskee";

		if (rand()%10/5 > this->stats.luck) {
			std::cout << "... mutta se epäonnistui." << std::endl;
		}
		else {
			if (opponent->dodging) {
				int damage = -10;

				std::cout << ". Vastustaja väisti ja " << this->name << " loukkasi itseään " << std::to_string(damage) << "HP" << std::endl;

				this->setHP(damage);
			}
			else {
				int a = (this->attackIndex % this->moves.size());
				Move *tmp = this->moves.at(a);

				int power = (*tmp).power;
				double coeff = this->type.coeff(this->type.id, opponent->type.id);
				double rnd = 0.5+rand()/1.7e9;
				double stats = ((double)this->stats.attack/opponent->stats.defence);
				int damage = -1 * rnd * power * coeff - stats;

				if (opponent->defending) damage = 0;

				std::cout << " liikkeellä " << ANSI::WEIGHT_BOLD << tmp->name << ANSI::WEIGHT_NORMAL << "! " <<
							 "Se aiheutti " << ANSI::WEIGHT_BOLD << std::to_string(damage) << ANSI::WEIGHT_NORMAL << "HP vahinkoa." << std::endl;

				opponent->setHP(damage);
			}
		}
	}
	else if (this->defending) std::cout << "estää" << std::endl;
	else if (this->dodging) std::cout << "väistää" << std::endl;

}

void Page::endRound() {

	this->attacking = false;
	this->defending = false;
	this->dodging = false;
	this->menu = false;

}

void Page::attack(int a) { this->attacking = true; this->attackIndex = a; }

void Page::defend() { this->defending = true; }

void Page::dodge() { this->dodging = true; }