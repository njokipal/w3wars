#ifndef PAGE_H
#define PAGE_H

#include <string>
#include <vector>
#include "URL.hpp"
#include "Stats.hpp"
#include "Type.hpp"
#include "Move.hpp"

class Page {

	public:
		Page();
		Page(std::string, int);
		~Page();

		friend std::ostream & operator<<(std::ostream &, const Page *);
		friend std::ostream & operator<<(std::ostream &, const Page &);

		void crawl();

		std::string strMin() const;
		std::string str(int) const;
		std::string getHP() const;
		std::string getPlayer() const;
		std::string getInfo() const;

		void setHP(int);

		void planRound(Page *);
		void battleRound(Page *);
		void endRound();
		void attack(int);
		void defend();
		void dodge();

		std::string name;
		int player;
		Type type;
		URL *location;
		STATS stats;

		std::vector<Move *> moves;

		int attackIndex;
		bool ready, menu, fainted, attacking, defending, dodging;

	private:
		double filesize;
		double loadtime;
		bool secure;

		std::string keywords;
		std::string description;

		void parseSource(const std::ostringstream &);
		void parseHeaders(const std::ostringstream &);
		void determineType();
		void determineStats();
		void determineMoves();

};

#endif // PAGE_H