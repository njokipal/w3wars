# w3wars eli www wars

C++-terminaaliohjelma (Linux, osittainen tuki myös Win), jossa kaksi pelaajaa tai pelaaja ja tietokone kamppailevat annettujen URL-osoitteiden perusteella luoduilla olioilla.

Ohjelma käyttää POSIX thread -säikeistystä kun cURL-kirjastolla taustalla haetaan ja parsitaan nettisivujen HTML-lähdekoodi.
Lähdekoodista tarkistetaan meta-tageista `description` ja `keywords`, joiden perusteella sivuston tyyppi päätellään (hakukone, viihdesivusto, videosivusto jne.). Myös ladatun lähdekoodin tiedostokoko ja latauksen kesto määrittelevät olioille ominaisuuksia.
Näytölle tulostetaan muotoiltua tekstiä ANSI-muotoilun avulla, jota tukee ainakin jossain määrin lähes kaikki UNIX-terminaalit.

Ohjelma on tehty Olio-ohjelmointi 2 -opintojakson harjoitustyönä 12/2014.

## Asennus ja käyttö

Ohjelma käyttää cURL-kirjastoa, joten asenna puuttuessa `$ sudo apt-get install curl`.

Koosta lähdekoodeista ajettava ohjelma

	$ g++ -O -std=c++11 *.cc -lcurl -pthread -lpthread -o w3wars

Käsky koostaa c++-lähdekoodit g++-kääntäjällä. Mukaan otetaan säikeistykseen (`-pthread -lpthread`) ja nettiliikenteen (`lcurl`) koodit käyttöjärjestelmän oletuspoluista.
Käännökseen jälkeen ilmestyy ajettava `w3wars`-tiedosto, jonka saa käyntiin käskyllä

	$ ./w3wars
